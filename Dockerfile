FROM debian:jessie
MAINTAINER Bruno Plamondon <plamondonb@sonatest.com>

# add repo for mxe
RUN echo "deb http://pkg.mxe.cc/repos/apt/debian/ jessie main" > /etc/apt/sources.list.d/mxe.list && \
  apt-key adv --keyserver keyserver.ubuntu.com --recv-keys D43A795B73B16ABE9643FE1AFD8FFF16DB45C6AB

RUN apt-get update

# install tools for compiling projects
RUN apt-get install -y --no-install-recommends git make

# install library QT4
RUN apt-get install -y --no-install-recommends mxe-x86-64-w64-mingw32.static-qt mxe-x86-64-w64-mingw32.shared-qt \
  mxe-i686-w64-mingw32.static-qt mxe-i686-w64-mingw32.shared-qt
  
# install library Eigen & GSL
RUN apt-get install -y --no-install-recommends mxe-i686-w64-mingw32.shared-eigen mxe-i686-w64-mingw32.static-eigen \
  mxe-x86-64-w64-mingw32.shared-eigen mxe-x86-64-w64-mingw32.static-eigen \
  mxe-i686-w64-mingw32.static-gsl mxe-x86-64-w64-mingw32.static-gsl

# installing vtk
RUN apt-get install -y --no-install-recommends mxe-i686-w64-mingw32.shared-vtk6 mxe-i686-w64-mingw32.static-vtk6 \
  mxe-x86-64-w64-mingw32.shared-vtk6 mxe-x86-64-w64-mingw32.static-vtk6

# install g++
RUN apt-get install -y --no-install-recommends mxe-i686-w64-mingw32.shared-gcc mxe-i686-w64-mingw32.static-gcc \
  mxe-x86-64-w64-mingw32.shared-gcc mxe-x86-64-w64-mingw32.static-gcc

# install pthreads
RUN apt-get install -y --no-install-recommends mxe-i686-w64-mingw32.shared-pthreads mxe-i686-w64-mingw32.static-pthreads \
  mxe-x86-64-w64-mingw32.shared-pthreads mxe-x86-64-w64-mingw32.static-pthreads

# cleanup
RUN apt-get clean && apt-get autoclean && apt-get autoremove

# create builder user
RUN useradd --uid 1000 -G sudo --create-home builder

# create user and add bin folder
ADD bin /home/builder/bin
RUN chown -R builder:builder /home/builder/bin
ENV PATH=/home/builder/bin:$PATH

# run pthreads patches
RUN pthreads_patch

USER builder

ENV PATH=/usr/lib/mxe/usr/bin/:$PATH

# Symbol to MXE's 32-bits qmake
ENV WIN32QMAKE=/usr/lib/mxe/usr/i686-w64-mingw32.shared/qt/bin/qmake
# Symbol to MXE's 64-bits qmake
ENV WIN64QMAKE=/usr/lib/mxe/usr/x86_64-w64-mingw32.shared/qt/bin/qmake

WORKDIR /devg3

